//
//  RMErrors.swift
//  RickMortyApp
//
//  Created by Maheep Kaushal on 10/02/22.
//

import Foundation

enum RMError: Error {
    case failedToGetDataFromAPICharacters
    case failedToGetDataFromAPI
}
