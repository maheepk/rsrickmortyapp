//
//  NetworkManager.swift
//  RickMortyApp
//
//  Created by Maheep Kaushal on 10/02/22.
//

import Foundation
import Alamofire

struct NetworkManager {

    static let shared: NetworkManager = NetworkManager()
    let session: Session

    init() {
        self.session = Session.default
    }

    public func fetch(_ convertible: URLRequestConvertible) -> DataRequest {
        NetworkManager.shared.session.request(convertible)
    }

    static func isNetworkReachable() -> Bool {
        guard let networkReachabilityManager = Alamofire.NetworkReachabilityManager() else {
            return true
        }
        return networkReachabilityManager.isReachable
    }
}
