//
//  RMRouter.swift
//  RickMortyApp
//
//  Created by Maheep Kaushal on 10/02/22.
//

import Foundation
import Alamofire

enum RMRouter: URLRequestConvertible {
    enum Constants {
        static let baseURLPath: String = "https://rickandmortyapi.com/api/"
    }

    case getCharacters

    var method: HTTPMethod {
        return .get
    }

    var path: String {
        switch self {
            case .getCharacters:
            return "character"
        }
    }

    var parameters: [String: Any] {
        return [:]
    }

    func asURLRequest() throws -> URLRequest {
        let url: URL = try Constants.baseURLPath.asURL()
        var request: URLRequest = URLRequest(url: url.appendingPathComponent(path))
        request.httpMethod = method.rawValue
        request.timeoutInterval = TimeInterval(10*1000)
        return try URLEncoding.default.encode(request, with: parameters)
    }

}
