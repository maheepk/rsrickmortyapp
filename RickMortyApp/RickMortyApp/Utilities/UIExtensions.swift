//
//  UIExtensions.swift
//  RickMortyApp
//
//  Created by Maheep Kaushal on 10/02/22.
//

import Foundation
import MBProgressHUD

extension UIViewController {
    func showIndicator(title: String, descriptionTitle: String? = nil, _ backgroundColor: UIColor = UIColor(white: 0.0, alpha: 0.1)) {
        let indicator: MBProgressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        indicator.backgroundView.style = MBProgressHUDBackgroundStyle.solidColor
        indicator.backgroundView.color = backgroundColor
        indicator.label.text = title
        indicator.label.numberOfLines = 3
        indicator.detailsLabel.text = descriptionTitle
        indicator.show(animated: true)
    }

    func hideIndicator() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}

extension UIView {
    func round(corners: UIRectCorner, cornerRadius: Double) {
        let size = CGSize(width: cornerRadius, height: cornerRadius)
        let bezierPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: size)
        let shapeLayer = CAShapeLayer()
        shapeLayer.frame = self.bounds
        shapeLayer.path = bezierPath.cgPath
        self.layer.mask = shapeLayer
    }
}
