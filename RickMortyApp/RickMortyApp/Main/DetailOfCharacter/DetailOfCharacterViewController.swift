//
//  DetailOfCharacterViewController.swift
//  RickMortyApp
//
//  Created by Maheep Kaushal on 10/02/22.
//

import UIKit
import MBProgressHUD
import SDWebImage

class DetailOfCharacterViewController: UIViewController {
 
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var speciesLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var currentLocationLabel: UILabel!
    
    var viewModel = CharacterViewModel()
    var selectedIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    func initView() {
        
        guard let index = selectedIndex else { return }

        self.profileImageView.sd_setImage(with: URL(string: viewModel.getImageUrl(index)), completed: nil)
        self.speciesLabel.text = viewModel.getSpecies(index)
        self.statusLabel.text = viewModel.getStatus(index)
        self.genderLabel.text = viewModel.getGender(index)
        self.nameLabel.text = viewModel.getNameValue(index)
        self.currentLocationLabel.text = viewModel.getCurrentLocation(index)
        
    }
}
