//
//  CharacterViewModel.swift
//  RickMortyApp
//
//  Created by Maheep Kaushal on 10/02/22.
//

import Foundation
import Alamofire

class CharacterViewModel {

    private var characters: [CharacterData] = [CharacterData]()
    private var allCharacters: [CharacterData] = [CharacterData]()

    func getListOfCharacters(completion: @escaping((Bool, Error?) -> Void)) {
        NetworkManager.shared.fetch(RMRouter.getCharacters).responseDecodable(of: CharacterModel.self) { response in
            guard let characterModelValue = response.value else { return completion(false, RMError.failedToGetDataFromAPI) }
            guard let getAllCharacters = characterModelValue.results else { return completion(false, RMError.failedToGetDataFromAPICharacters) }

            // Handle Error Cases
            //
            self.characters = getAllCharacters
            self.allCharacters = getAllCharacters
            completion(true, nil)
        }
    }
    
    func searchTextWithName(_ searchStr: String) {
        
        guard searchStr.count > 0 else {
            characters = self.allCharacters
            return
        }

        characters = self.allCharacters.filter { $0.name?.contains(searchStr) == true }
    }
    
    func numberOfRows() -> Int {
        characters.count
    }
    
    func getNameValue(_ index: Int) -> String {
        guard let name = self.characters[index].name else { return "" }
        return "Name: \(name.capitalized)"
    }
    
    func getImageUrl(_ index: Int) -> String {
        guard let imageUrl = self.characters[index].image else {return ""}
        return imageUrl
    }
    
    func getListOfEpisodes(_ index: Int) -> String {
        guard let listOfEpisos = self.characters[index].episode else {return ""}
        
//        var episodes = ""
//        for localEpisode in listOfEpisos {
//            episodes += "\n\(localEpisode)"
//        }
        return "Number of Episodes: \(listOfEpisos.count)"
    }
    
    
    func getSpecies(_ index: Int) -> String {
        guard let specie = self.characters[index].species else {return ""}
        return "Species: \(specie)"
    }

    func getStatus(_ index: Int) -> String {
        guard let status = self.characters[index].status else {return ""}
        return "Status: \(status)"
    }

    func getGender(_ index: Int) -> String {
        guard let gender = self.characters[index].gender else {return ""}
        return "Gender: \(gender)"
    }

    func getCurrentLocation(_ index: Int) -> String {
        guard let locationName = self.characters[index].location?.name else {return ""}
        return "Current Location: \(locationName)"
    }
}
