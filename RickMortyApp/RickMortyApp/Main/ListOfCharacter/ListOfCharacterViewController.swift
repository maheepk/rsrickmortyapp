//
//  ListOfCharacterViewController.swift
//  RickMortyApp
//
//  Created by Maheep Kaushal on 10/02/22.
//

import UIKit
import MBProgressHUD
import SDWebImage

class ListOfCharacterViewController: UIViewController {

    var viewModel: CharacterViewModel = CharacterViewModel()

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "List of Characters"
        
        self.showIndicator(title: "")
        viewModel.getListOfCharacters { success, errors in
            DispatchQueue.main.async {
                self.hideIndicator()
                self.tableView.reloadData()
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "DetailSegue" {
            
            if let destinationViewController = segue.destination as? DetailOfCharacterViewController {

                guard let indexPath = self.tableView.indexPathForSelectedRow else { return }
                let index = indexPath.row
                destinationViewController.viewModel = viewModel
                destinationViewController.selectedIndex = index
            }
        }
    }
}

extension ListOfCharacterViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell: ListOfCharacterTableViewCell = tableView.dequeueReusableCell(withIdentifier: ListOfCharacterTableViewCell.identifier, for: indexPath) as? ListOfCharacterTableViewCell else {
            return UITableViewCell()
        }
        
        let index = indexPath.row
        
        cell.nameLabel.text = viewModel.getNameValue(index)
        cell.profileImageView.sd_setImage(with: URL(string: viewModel.getImageUrl(index)), completed: nil)
        cell.listOfEpisodeLabel.text = viewModel.getListOfEpisodes(index)
        return cell
    }
}

extension ListOfCharacterViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        viewModel.searchTextWithName(searchText)
        self.tableView.reloadData()
        
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
