//
//  ListOfCharacterTableViewCell.swift
//  RickMortyApp
//
//  Created by Maheep Kaushal on 10/02/22.
//

import UIKit

class ListOfCharacterTableViewCell: UITableViewCell {

    static let identifier: String = String(describing: ListOfCharacterTableViewCell.self)

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var listOfEpisodeLabel: UILabel!
}
